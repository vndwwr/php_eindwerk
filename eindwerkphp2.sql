-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 24, 2018 at 07:57 PM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eindwerkphp2`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `naam` varchar(255) NOT NULL,
  `omschrijving` varchar(500) NOT NULL,
  `prijs` decimal(10,0) NOT NULL,
  `btwtarief` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `naam`, `omschrijving`, `prijs`, `btwtarief`, `image`) VALUES
(1, 'Bergeiri', 'Bergeri has a striking set of leaves that grow upwards, so it looks like the leaves are twirling and dancing around the stem of the plant!\r\n\r\nThis is a hardy plant in a deep sage green colour. Bergeri will get a bright red spike when in bloom with beautiful purple flowers. It''s also called "king of the clumps" since this variety quickly and easily forms clumps.\r\n\r\nThis plant is native to Argentina, so it''s used to warm, sunny and humid temperatures. ', '12', 21, 'assets/images/bergeri.jpg'),
(2, 'Gracilis', 'Gracilis is a very graceful plant, as her name already points out. Just like Filifolia, this plant has very thin leaves so it looks super fluffy and cute.\r\n\r\nGracilis originally grows in the woodlands of Mexico, so she has grey green leaves and requires bright light. Due to her thin leaves, it''s necessary to mist her often so she doesn''t dry out. ', '7', 21, 'assets/images/gracilis.jpg'),
(3, 'Paleacea', 'Paleacea has a very funky look, with spikes that grow from the middle of the plant. The leaves are super soft and almost seem to be white because of all the tiny fuzzy trichomes.  \r\n\r\nThis airplant has a tremendous capacity to absorb water from the air, so it requires much less watering than most airplants. Paleacea produces violet flowers when in bloom. ', '14', 21, 'assets/images/paleacea.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
