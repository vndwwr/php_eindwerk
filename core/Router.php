<?php
    class Router{
        protected $routes = [
            'GET' => [],
            'POST' => []
        ];

        public function get($uri,$controller){
            $this->routes['GET'][$uri] = 'controllers/'.$controller;
        }

        public function post($uri,$controller){
            $this->routes['POST'][$uri] = 'controllers/'.$controller;
        }

        //kijken of bestaat, functie aanmaken om 'verkeer' te regelen
        //function krijgt uri en method binnen
        //gaat dan kijken of de uri ergens in de route staat, bestaat element in routes? Dus met een if
        // $this->routes[$method] : gaat zoeken in ofwel de post of de get helft -> method! indien key uri bestaat in de method waar we zitten, dan...

        public function direct($uri,$method){

            if(array_key_exists($uri, $this->routes[$method])){
                return $this->routes[$method][$uri];
                //this->routes is routes: protected, de method geeft post of get en de uri geeft een bepaald element in de post of get terug
                //vb routes[GET];[] : je krijgt de homepage terug
            }
             else{
                 //die('Deze route bestaat niet');
                 throw new Exception('Deze route bestaat niet',1);
            }
        }
    }
?>