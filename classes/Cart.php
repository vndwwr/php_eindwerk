<?php
    class Cart{
    public $contents = [];

        public function addToCart(Product $product,$count=1) {
          $id=$product->id;
          if (!array_key_exists($id,$this->contents)) {
            $this->contents[$id]=$count;
          }else{
            $this->contents[$id]+=$count;
          }
        }

        public function setAmount($product_id,$count){
          if (array_key_exists($product_id,$this->contents)) {
            $this->contents["$product_id"]=$count;
          }
        }

        public function removeFromCart($product_id,$count){
          if (array_key_exists($product_id,$this->contents)) {
            $this->contents["$product_id"]=$count;
          }
        }
      
  			public function delete($id){
            unset($this->contents[$id]);
        }
    }