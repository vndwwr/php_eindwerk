<?php 

class Product {
    public $id;
    public $naam;
    public $omschrijving;
    public $prijs;
    public $btwtarief;
    public $image;


    public function prijsinclusiefbtw(){
		return $this->prijs/100*($this->btwtarief)+$this->prijs;
	}

}