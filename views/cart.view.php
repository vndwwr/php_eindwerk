<?php require('includes/htmltop.php'); ?>

<form method="post">
    <h1>Shopping Cart</h1>
      <div class="row center">
        <div class="col-md-12">
          <h4 class="d-flex justify-content-between align-items-center mb-12">
            <span class="text-muted">Your cart</span>
          </h4>

    <?php foreach($cart->contents as $id=>$amount) { 
               $product = $query->selectwithid("products", $id);
               ?>
          <ul class="list-group mb-12">
            <li class="list-group-item d-flex justify-content-between lh-condensed">
            <h6 class="my-0"><?= $product->naam ;?></h6>
            <p><?= $amount ?></p>  
              <div>
              <form action="<?= url('cart/update') ?>" method="post">     
                  <a href="<?= url('cart/add?id='.$product->id) ;?>" class="btn btn-light">+</a>
                  <a href="<?= url('cart/remove?id='.$product->id) ;?>" class="btn btn-light">-</a>
                  <a href="<?= url('cart/delete?id='.$product->id) ;?>" class="btn btn-light">Delete</a>          
              </form>
              </div>
              <span class="text-muted">Prijs incl btw: € <?= $product->prijsinclusiefbtw()*$amount ;?></span>
            </li>
             <?php }; ?>

            <hr class="mb-12">


           <a href="<?= url('cart/empty') ;?>" class="btn btn-light btn-lg btn-block container empty">Check out</a>
          </form>


<?php require('includes/htmlbottom.php'); 

