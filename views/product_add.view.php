<?php require('includes/htmltop.php'); ?>


<section class="container cards">
  <div class="title-head">
    <h1 class="text-center">Add a new product</h1>
  </div>

  <form class="addproduct-form" method="post" action="<?= url('product/save'); ?>">
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="naam">
  </div>
  <div class="form-group">
    <label>Description</label>
    <input type="text" class="form-control" name="omschrijving">
  </div>
  <div class="form-group">
    <label >Price</label>
    <input type="text" class="form-control" name="prijs">
  </div>
  <div class="form-group">
    <label>BTW</label>
    <input type="text" class="form-control" name="btwtarief" placeholder="21">
  </div>
  <div class="form-group">
    <label>Images</label>
    <input type="text" class="form-control" name="image" placeholder="assets/images/number1-13.jpg">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
  
</form>


  <?php require('includes/htmlbottom.php'); ?>