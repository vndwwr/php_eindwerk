<?php require('includes/htmltop.php'); ?>


<section class="container cards">
    <div class="title-head">
        <h1 class="text-center">All products</h1>
    </div>

    <div class="card">

        <?php
foreach ($products as $product) {
    ?>

        <div class="card-body">
            <img class="card-img-top" src="<?php echo $product->image; ?>" alt="Card image cap">
            <h5 class="card-title text-center">
                <?php echo $product->naam; ?>
            </h5>
            <p class="card-text text-center"> 
                € <?php echo $product->prijs; ?> excl. btw
            </p>
            <a href="<?= url('product/detail?id='.$product->id) ;?>" class="btn btn-light">Take a look</a>
            <a href="<?= url('product/edit?id='.$product->id) ;?>" class="btn btn-light">Edit</a>
            <a href="<?= url('product/delete?id='.$product->id) ;?>" class="btn btn-light">Delete</a>
        </div>
        <?php
    }
    ?>
    </div>

</section>

<?php require('includes/htmlbottom.php'); 