<?php require 'includes/htmltop.php'; ?>


<div class="card mb-3">
  <img class="card-img-top" src="<?= url($product->image) ;?>?>" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title"><?php echo $product->naam; ?></h5>
    <p class="card-text"><?php echo $product->omschrijving; ?></p>
    <p class="card-text"><small class="text-muted"> € <?php echo $product->prijs;?> excl. btw</small></p>
    <a href="<?= url('cart/add?id='.$product->id) ;?>" class="btn btn-secondary">Add to cart</a>
  </div>
</div>

<?php require 'includes/htmlbottom.php'; ?>