<?php

function dd($var){
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
    die();
}

function url($relatieveurl){
    return "http://".$_SERVER['HTTP_HOST'].'/'.$relatieveurl;
}


function redirect($relatieveurl){
    return header("Location: ". url($relatieveurl));
}