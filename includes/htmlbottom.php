<footer class="my-5 pt-5 text-muted text-center text-small">
  <p class="mb-1">&copy; 2018 - Eindwerk PHP-OOP - Shana Vandewouwer</p>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?= url('js/bootstrap.min.js');?>"></script>
</body>

</html>