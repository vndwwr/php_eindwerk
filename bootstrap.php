<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
require 'classes/Product.php';
require 'classes/Cart.php';

session_start();

require 'database/Connection.php';
require 'database/Querybuilder.php';
require 'includes/functions.inc.php';
require 'core/Router.php';
require 'core/Request.php';

//start cart
if(!isset($_SESSION['cart'])){
    $_SESSION['cart'] = new Cart();
} else {
    $cart = $_SESSION['cart'];
}

$pdo = Connection::make();
$query = new Querybuilder($pdo);

$router = new Router;
require 'routes.php';
