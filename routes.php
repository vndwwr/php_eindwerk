<?php

$router ->get('','product_list.php');
$router ->get('products','product_list.php');
$router ->get('product/add','product_add.php');
$router ->get('product/edit','product_edit.php');
$router ->get('product/detail','product_detail.php');
$router ->get('product/delete','product_delete.php');
$router ->get('cart/add','cart_add.php');
$router ->get('cart/remove','cart_remove.php');
$router ->get('cart', 'cart.php');
$router ->get('cart/update','cart_update.php');
$router ->get('cart/empty','cart_empty.php');
$router ->get('cart/delete','cart_itemdelete.php');

$router ->post('product/save','product_save.php');
$router ->post('product/update','product_update.php');






