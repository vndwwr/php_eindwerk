<?php
class Querybuilder{

    public $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    public function selectAll($table){
        $classname = substr(ucfirst($table),0,-1);

        $stmt = $this->pdo->prepare("SELECT * FROM $table");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,$classname);
    }

    public function selectwithid($table,$id){
        $classname = substr(ucfirst($table),0,-1);

        $stmt = $this->pdo->prepare("SELECT * FROM $table WHERE id=$id");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,$classname)[0];
    }

    public function selectwithcolumn($table,$kolom,$waarde){
        $classname = substr(ucfirst($table),0,-1);

        $stmt = $this->pdo->prepare("SELECT * FROM $table WHERE $kolom=$waarde");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS,$classname)[0];
    }

    public function delete($table,$id){
        $stmt = $this->pdo->prepare("DELETE FROM $table WHERE id=$id");
        $stmt->execute();
    }

    public function insert($table,$parameters){
         $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s)",
            $table,
            implode(',',array_keys($parameters)),
            ":".implode(',:',array_keys($parameters))
        );

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($parameters);

    }

    public function update($table,$id,$parameters){
        $string = "";
        foreach($parameters as $key=>$value){
            $string .= $key . '= :' . $key.', ';
        }

        $sql = sprintf(
            "UPDATE %s SET %s WHERE id = %s",
            $table,
            substr($string,0,-2),
            $id
        );
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($parameters);

    }

}



















